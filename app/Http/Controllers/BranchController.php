<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Branches;
use App\Http\Requests\CreateBranchRequest;
use App\Http\Controllers\Controller;

class BranchController extends Controller
{
    
    /**
     * Security checkpoint.
     *
     * @return Response
     */
    public function __construct()
    {

        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return view('branch.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(CreateBranchRequest $request)
    {
        
        $branches = new Branches;
        $branches->store_id = $request->store_id;
        $branches->address  = $request->address;
        $branches->landmark = $request->landmark;
        $branches->store_hours = $request->store_hours;
        $branches->telephone_number = $request->telephone_number;

        $branches->save();

        \Session::flash('flash_message', 'Branch has been added.');
        
        return redirect('branch/create');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}