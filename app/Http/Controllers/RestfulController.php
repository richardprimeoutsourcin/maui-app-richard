<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App;
use Auth;
use Validator;
use App\Http\Requests;
use App\Http\Requests\CreateCheckInRequest;
use App\Http\Controllers\Controller;

class RestfulController extends Controller
{
    

    /**
     * Json return header
     *
     * @return Response
     */
    public function jsonFileHeader() 
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Request-With');
        header('Access-Control-Allow-Credentials: true');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function authenticate(Request $request)
    {   

        $email = $request->email;
        $password = $request->password;

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            $response = ['authenticate' => 'true'];
        } else {
            $response = ['authenticate' => 'false'];
        }

        // Return the json file header.
        $this->jsonFileHeader();
        
        return $response;
    }


    /**
     * Display the _branches.
     *
     * @return Response
     */
    public function showBranches($id = null)
    {
        $branchesWithStores = \DB::table('branches AS b')
        ->leftJoin('stores AS st', 'st.store_id', '=', 'b.store_id')
        ->select(
            'b.branch_id',
            'b.store_id',
            'b.address', 
            'b.landmark', 
            'b.store_hours', 
            'b.telephone_number',
            'st.name AS store_name'
        )
        ->where('b.branch_id', $id)
        ->get();

        $branchesWithServices = \DB::table('services_offer AS so')
        ->leftJoin('services AS s', 'so.service_id', '=', 's.service_id')
        ->select('s.service_id', 's.name')
        ->where('so.branch_id', $id)
        ->get();

        $branchesWithStoresAndServices = array('branches' => $branchesWithStores, 'services' => $branchesWithServices);

        // Return the json file header
        $this->jsonFileHeader();

        return $branchesWithStoresAndServices;
    }


    /**
     * Display the _services.
     *
     * @return Response
     */
    public function showServices($id = null)
    {   
        if (empty($id)) {
            $services = App\Services::all();
        } else {
            $services = App\Services::findOrFail($id);
        }

        // Return the json file header.
        $this->jsonFileHeader();

        return $services;
    }


    /**
     * Display the _stores.
     *
     * @return Response
     */
    public function showStores($id = null)
    {
        
        if (empty($id)) {
            $stores = App\Stores::all();    
        } else {
            $stores = App\Stores::findOrFail($id);
        }

        // Return the json file header.
        $this->jsonFileHeader();

        return $stores;
    }


    /**
     * Display the _branches which associated with _services.
     *
     * @var $id unique key of _services
     * @todo It should be eloquent not \DB
     * @return Response
     */
    public function showServicesWithBranches($id = null)
    {
        $servicesWithBranches = \DB::table('services_offer AS so')
        ->join('branches AS b', 'so.branch_id', '=', 'b.branch_id')
        ->leftJoin('stores AS st', 'st.store_id', '=', 'b.store_id')
        ->leftJoin('services AS s', 'so.service_id', '=', 's.service_id')
        ->select(
            'b.branch_id',
            'b.store_id',
            'b.address', 
            'b.landmark', 
            'b.store_hours', 
            'b.telephone_number', 
            's.name AS service_name', 
            'st.name AS store_name'
        )
        ->where('so.service_id', $id)
        ->get();

        // Return the json file header.
        $this->jsonFileHeader();

        return $servicesWithBranches;
    }


    /**
     * Store a newly created check_ins
     *
     * @param  Request  $request
     * @return Response
     */
    public function storeCheckIns(Request $request)
    {
        // Validate the data.
        // Used manual validator intead of Requests/CheckInsRequest so we can return the response to json.
        $validator = Validator::make($request->all(), [
            'branch_id'  => 'required',
            'service_id' => 'required',
            'name'       => 'required|min:3',
            'email'      => 'required|email',
            'phone_no'   => 'required|min:3',
            'no_of_guest'=> 'required|numeric',
        ]);

        // If validator fails.
        if ($validator->fails()) {
            $response = ['status' => 'failed', 'message' => $validator->errors()];
        // No errors, just continue.
        } else {

            $checkIns = new App\CheckIns;
            $checkIns->branch_id    = $request->branch_id;
            $checkIns->service_id   = $request->service_id;
            $checkIns->name         = $request->name;
            $checkIns->email        = $request->email;
            $checkIns->phone_no     = $request->phone_no;            
            $checkIns->no_of_guest  = $request->no_of_guest;
            
            if ($checkIns->save()) {
                $response = ['status' => 'success', 'message' => 'Successfully Check In!'];
            } else {
                $response = ['status' => 'failed', 'message' => 'Database Error'];
            }
        }

        // Return the json file header.
        $this->jsonFileHeader();

        return $response;
    }
}