## Maui Super Saloon Mobile App

Maui Whitening understands that with today’s fast-paced lifestyle, you need a teeth whitening salon that fits your schedule and is affordable. That’s why we accept walk-in clients. Teeth whitening sessions last from 20-60 minutes in length.  At Maui Whitening, you can relax, because you know you will receive quality products and equipment, to produce an overall effective whiter smile.  We use only professional-grade  whitening products. We offer professional teeth whitening sessions at a fraction of the cost of what you would pay to have your teeth whitened at other locations and we use the same grade equipment and products used in dental offices across the country.

Putting convenience directly into our customer’s hands, Maui Whitening launched the industry’s first  Online Check-In  service, allowing customers to add their name to the wait list of a Maui Whitening salon—before they arrive.

Maui Whitening’s mission is to make teeth whitening affordable, convenient and effective.   We currently have salons located throughout different parts of the US and many more scheduled to open soon. 

### Contributor
Jemuel Rodriguez  
Richard Abela  
Marc Jesus Caabay  
Mark Joseph Marquez  
Barry Hawkins  
Diomdes Jaojoco  
Mandy Relente  
Zhandra Habig  
Marlon Diaz  
Sheeraz Faheem  
Sydney Reyes  

### API Sample request
1. rest/services  
rest/services/{id}  

2. rest/branches  
rest/branches/{id}  

3. rest/stores  
rest/stores/{id}  

4. rest/services-with/braches{id}  
where {id} is the unique key of services  

5. rest/check-ins
post/store request data to check ins  