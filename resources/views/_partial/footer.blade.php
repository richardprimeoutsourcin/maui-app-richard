    <!--footer start-->
    <footer class="site-footer">
        <div class="text-center">
            2015 &copy; Maui Super Saloon.
            <a href="#" class="go-top">
                <i class="fa fa-angle-up"></i>
            </a>
        </div>
    </footer>
    <!--footer end-->